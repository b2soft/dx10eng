#pragma once

#include "CoreHeader.h"

class CCamera
{
public:
	CCamera();
	CCamera(const CCamera &);
	~CCamera();

	void SetPosition(float, float, float);
	void SetPosition(D3DXVECTOR3 &);
	void SetRotation(float, float, float);
	void SetRotaton(D3DXVECTOR3 &);

	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetRotation();

	void Render();
	void GetViewMatrix(D3DXMATRIX &);

private:
	D3DXVECTOR3 m_pos;
	D3DXVECTOR3 m_rotation;
	D3DXMATRIX m_viewMatrix;
};

