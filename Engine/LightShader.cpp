#include "LightShader.h"

CLightShader::CLightShader()
{
	m_effect = 0;
	m_technique = 0;
	m_layout = 0;

	m_pProjMatrix = 0;
	m_pWorldMatrix = 0;
	m_pViewMatrix = 0;
	m_pTexture = 0;

	m_pLightDirection = 0;
	m_pAmbientColor = 0;
	m_pDiffuseColor = 0;

	m_pCameraPosition = 0;
	m_pSpecularColor = 0;
	m_pSpecularPower = 0;
}

CLightShader::CLightShader(const CLightShader &)
{

}

CLightShader::~CLightShader()
{

}

bool CLightShader::Initialize(ID3D10Device *device, HWND hwnd)
{
	bool result = true;

	result = InitalizeShader(device, hwnd, L"Resources/Light.fx");
	return result;
}

void CLightShader::Shutdown()
{
	ShutdownShader();
}

void CLightShader::Render(ID3D10Device *device, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projMatrix, ID3D10ShaderResourceView *texture,
	D3DXVECTOR3 lightDirection, D3DXVECTOR4 ambientColor, D3DXVECTOR4 diffuseColor, D3DXVECTOR3 cameraPosition, D3DXVECTOR4 specularColor, float specularPower)
{
	//set matrices for shader
	SetShaderParametres(worldMatrix, viewMatrix, projMatrix, texture, lightDirection, ambientColor, diffuseColor, cameraPosition, specularColor, specularPower);

	//render it!
	RenderShader(device, indexCount);
}

bool CLightShader::InitalizeShader(ID3D10Device *device, HWND hwnd, WCHAR *filename)
{
	HRESULT hr;
	ID3D10Blob *errorMessage;
	D3D10_INPUT_ELEMENT_DESC polygonLayout[3];
	unsigned int numElements;
	D3D10_PASS_DESC passDesc;

	errorMessage = 0;

	//Load shader from file
	hr = D3DX10CreateEffectFromFile(filename, NULL, NULL, "fx_4_0", D3D10_SHADER_ENABLE_STRICTNESS, 0,
		device, NULL, NULL, &m_effect, &errorMessage, NULL);

	if (FAILED(hr))
	{
		if (errorMessage)
			OutputShaderErrorMsg(errorMessage, hwnd, filename);
		else
			MessageBox(hwnd, filename, L"Missing shader file", MB_OK);
		return false;
	}

	//get a pointer to technique within shader
	m_technique = m_effect->GetTechniqueByName("LightTechnique");
	if (!m_technique)
		return false;

	// Now setup the layout of the data that goes into the shader.
	// This setup needs to match the SVertexType structure in the CModel and in the shader.
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D10_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D10_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D10_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	polygonLayout[2].SemanticName = "NORMAL";
	polygonLayout[2].SemanticIndex = 0;
	polygonLayout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[2].InputSlot = 0;
	polygonLayout[2].AlignedByteOffset = D3D10_APPEND_ALIGNED_ELEMENT;
	polygonLayout[2].InputSlotClass = D3D10_INPUT_PER_VERTEX_DATA;
	polygonLayout[2].InstanceDataStepRate = 0;


	// Get a count of the elements in the layout.
	numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	// Get the description of the first pass described in the shader technique.
	m_technique->GetPassByIndex(0)->GetDesc(&passDesc);

	// Create the input layout.
	hr = device->CreateInputLayout(polygonLayout, numElements, passDesc.pIAInputSignature, passDesc.IAInputSignatureSize, &m_layout);
	if (FAILED(hr))
		return false;

	// Get pointers to the three matrices inside the shader so we can update them from this class.
	m_pWorldMatrix = m_effect->GetVariableByName("worldMatrix")->AsMatrix();
	m_pViewMatrix = m_effect->GetVariableByName("viewMatrix")->AsMatrix();
	m_pProjMatrix = m_effect->GetVariableByName("projectionMatrix")->AsMatrix();

	//get pointer to texture
	m_pTexture = m_effect->GetVariableByName("shaderTexture")->AsShaderResource();

	
	m_pLightDirection = m_effect->GetVariableByName("lightDirection")->AsVector();
	m_pAmbientColor = m_effect->GetVariableByName("ambientColor")->AsVector();
	m_pDiffuseColor = m_effect->GetVariableByName("diffuseColor")->AsVector();

	// Get pointers to the specular light components inside the shader.
	m_pCameraPosition = m_effect->GetVariableByName("cameraPosition")->AsVector();
	m_pSpecularColor = m_effect->GetVariableByName("specularColor")->AsVector();
	m_pSpecularPower = m_effect->GetVariableByName("specularPower")->AsScalar();


	return true;
}

void CLightShader::ShutdownShader()
{
	// Release the light pointers.
	m_pCameraPosition = 0;
	m_pSpecularColor = 0;
	m_pSpecularPower = 0;
	m_pLightDirection = 0;
	m_pDiffuseColor = 0;
	m_pAmbientColor = 0;

	//release pointers
	m_pWorldMatrix = 0;
	m_pViewMatrix = 0;
	m_pProjMatrix = 0;

	m_pTexture = 0;

	//release shader layout
	if (m_layout)
	{
		m_layout->Release();
		m_layout = 0;
	}

	//release shader ptr
	if (m_effect)
	{
		m_effect->Release();
		m_effect = 0;
	}
}

void CLightShader::OutputShaderErrorMsg(ID3D10Blob *errorMessage, HWND hwnd, WCHAR *shaderFilename)
{
	char *compileErrors;
	unsigned long bufferSize;
	ofstream fout;

	// Get a pointer to the error message text buffer.
	compileErrors = (char *)(errorMessage->GetBufferPointer());

	// Get the length of the message.
	bufferSize = errorMessage->GetBufferSize();

	// Open a file to write the error message to.
	fout.open("shader_error.txt");

	for (unsigned long i = 0; i < bufferSize; i++)
		fout << compileErrors[i];

	//close file
	fout.close();

	errorMessage->Release();
	errorMessage = 0;

	//pop an error msg
	MessageBox(hwnd, L"Error compiling shader. Check shader_error.txt", shaderFilename, MB_OK);
}

void CLightShader::SetShaderParametres(D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projMatrix, ID3D10ShaderResourceView *texture,
	D3DXVECTOR3 lightDirection, D3DXVECTOR4 ambientColor, D3DXVECTOR4 diffuseColor, D3DXVECTOR3 cameraPosition, D3DXVECTOR4 specularColor, float specularPower)
{
	//set the globals (matrices) in shader
	m_pWorldMatrix->SetMatrix((float *)&worldMatrix);
	m_pViewMatrix->SetMatrix((float *)&viewMatrix);
	m_pProjMatrix->SetMatrix((float *)&projMatrix);

	//bind texture
	m_pTexture->SetResource(texture);

	//set shader vars
	m_pLightDirection->SetFloatVector((float *)&lightDirection);
	m_pAmbientColor->SetFloatVector((float *)&ambientColor);
	m_pDiffuseColor->SetFloatVector((float *)&diffuseColor);

	m_pCameraPosition->SetFloatVector((float *)&cameraPosition);
	m_pSpecularColor->SetFloatVector((float *)&specularColor);
	m_pSpecularPower->SetFloat(specularPower);
}

void CLightShader::RenderShader(ID3D10Device *device, int indexCount)
{
	D3D10_TECHNIQUE_DESC techniqueDesc;

	//set input layout
	device->IASetInputLayout(m_layout);

	//get desc from shader
	m_technique->GetDesc(&techniqueDesc);

	//go through each pass
	for (unsigned int i = 0; i < techniqueDesc.Passes; ++i)
	{
		m_technique->GetPassByIndex(i)->Apply(0);
		device->DrawIndexed(indexCount, 0, 0);
	}
}
