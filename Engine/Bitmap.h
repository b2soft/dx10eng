#pragma once

#include "CoreHeader.h"

class CTexture;

class CBitmap
{
public:
	CBitmap();
	CBitmap(const CBitmap &);
	~CBitmap();

	bool Initialize(ID3D10Device *, int, int, WCHAR *, int, int);
	void Shutdown();
	bool Render(ID3D10Device *, int, int);//The Render function puts the model geometry on the video card to prepare it for drawing by the color shader

	int GetIndexCount() const;
	ID3D10ShaderResourceView *GetTexture();

private:
	bool InitializeBuffers(ID3D10Device *);
	void ShutdownBuffers();
	bool UpdateBuffers(int, int);
	void RenderBuffers(ID3D10Device *);

	ID3D10Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;

	bool LoadTexture(ID3D10Device *, WCHAR *);
	void ReleaseTexture();

	CTexture *m_texture;

	struct SVertexType //must match shader type!
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};

	int m_screenWidth, m_screenHeight;
	int m_bitmapWidth, m_bitmapHeight;
	int m_prevPosX, m_prevPosY;
};

