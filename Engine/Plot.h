#pragma once

#include "CoreHeader.h"
#include <unordered_map>
#include <string>

class CLine;

class CPlot
{
public:
	CPlot();
	CPlot(const CPlot &);
	~CPlot();

	bool Initialize(ID3D10Device *device, HWND hwnd, D3DXVECTOR3 pos = { 0.0f, 0.0f, 0.0f }, D3DXVECTOR3 size = { 10.0f, 10.0f, 10.0f }, double scale = 10.0f);
	void Shutdown();
	void Render(D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix);

	void AddLine(string name, D3DXVECTOR3 f1, D3DXVECTOR3 f2, D3DXVECTOR4 color, bool base = false);
private:

	void AddMarks();

	HWND m_hwnd;
	ID3D10Device *m_device;

	CLineShader *m_lineShader;
	unordered_map <string, CLine> *m_lines;

	D3DXVECTOR3 m_pos, m_size;
	double m_scale;
	void BuildFunc(double a, double b, double(*f)(double));
};