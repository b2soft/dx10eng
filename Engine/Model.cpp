#include "Model.h"

CModel::CModel()
{
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	m_texture = 0;

	m_model = 0;
}

CModel::CModel(const CModel &)
{

}

CModel::~CModel()
{
}

bool CModel::Initialize(ID3D10Device *device, char *modelFileName, WCHAR *textureFileName)
{
	bool result = true;
	//Load model data
	result = LoadModel(modelFileName);
	if (!result)
		return false;

	result = InitializeBuffers(device);// Initialize the vertex and index buffer that hold the geometry for triangles
	if (!result)
		return false;

	result = LoadTexture(device, textureFileName);
	if (!result)
		return false;

	return true;
}

void CModel::Shutdown()
{
	ReleaseTexture();
	ShutdownBuffers();
	ReleaseModel();
}

void CModel::Render(ID3D10Device *device)
{
	RenderBuffers(device);
}

int CModel::GetIndexCount() const
{
	return m_indexCount;
}

bool CModel::InitializeBuffers(ID3D10Device *device)
{
	SVertexType *vertices;
	unsigned long *indices;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT hr;

	//create vertex arr
	vertices = new SVertexType[m_vertexCount];
	if (!vertices)
		return false;

	indices = new unsigned long[m_indexCount];
	if (!indices)
		return false;

	//load dat to vertex+index arrays
	for (int i = 0; i < m_vertexCount; i++)
	{
		vertices[i].position = D3DXVECTOR3(m_model[i].x, m_model[i].y, m_model[i].z);
		vertices[i].texture = D3DXVECTOR2(m_model[i].tu, m_model[i].tv);
		vertices[i].normal = D3DXVECTOR3(m_model[i].nx, m_model[i].ny, m_model[i].nz);
		indices[i] = i;
	}

	//set up vertex buf desc
	vertexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(SVertexType)* m_vertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;

	//create vertex buffer
	hr = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if (FAILED(hr))
		return false;

	//set up index buf desc
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long)* m_indexCount;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;

	//create index buffer	
	hr = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if (FAILED(hr))
		return false;

	//after creating buffers - we not need them as arrays, delete it!
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;

	return true;
}

void CModel::ShutdownBuffers()
{
	//release the buffers
	if (m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	if (m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}
}

void CModel::RenderBuffers(ID3D10Device *device)
{
	//Purpose of function: set the vertex buffer and index buffer as active on the input assembler in the GPU
	unsigned int stride;
	unsigned int offset;

	//set stride and offset
	stride = sizeof(SVertexType);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	device->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	//set primitives in vertex buffer as triangles list
	device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

ID3D10ShaderResourceView * CModel::GetTexture()
{
	return m_texture->GetTexture();
}

bool CModel::LoadTexture(ID3D10Device *device, WCHAR *fileName)
{
	bool result;

	//create texture obj
	m_texture = new CTexture;
	if (!m_texture)
		return false;

	//init obj texture
	result = m_texture->Initialize(device, fileName);
	if (!result)
		return false;

	return true;
}

void CModel::ReleaseTexture()
{
	if (m_texture)
	{
		m_texture->Shutdown();
		delete m_texture;
		m_texture = 0;
	}
}

bool CModel::LoadModel(char *filename)
{
	ifstream fin;
	char input;

	fin.open(filename);
	if (fin.fail())
		return false;

	fin.get(input);
	while (input != ':') //find vertex num separator
		fin.get(input);

	//read vertex count
	fin >> m_vertexCount;
	m_indexCount = m_vertexCount; //set num indexes as vertexes num

	//create model using vertex count and data in file
	m_model = new SModelType[m_vertexCount];
	if (!m_model)
		return false;

	//read beginning of data
	fin.get(input);
	while (input != ':')
		fin.get(input);
	fin.get(input);
	fin.get(input);

	//read vertices
	for (int i = 0; i < m_vertexCount; i++)
	{
		fin >> m_model[i].x >> m_model[i].y >> m_model[i].z;
		fin >> m_model[i].tu >> m_model[i].tv;
		fin >> m_model[i].nx >> m_model[i].ny >> m_model[i].nz;
	}
	fin.close();

	return true;
}

void CModel::ReleaseModel()
{
	if (!m_model)
	{
		delete[] m_model;
		m_model = 0;
	}
}
