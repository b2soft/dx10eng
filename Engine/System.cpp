#include "System.h"

CSystem::CSystem()
{
	m_Input = 0;
	m_Graphics = 0;

	m_Fps = 0;
	m_Cpu = 0;
	m_Timer = 0;
}

CSystem::CSystem(const CSystem&)
{
	//default copy constructor
}

CSystem::~CSystem()
{
}

bool CSystem::Initialize()
{
	int screenWidth, screenHeight;
	bool result;

	// Initialize the width and height of the screen to zero before sending the variables into the function.
	screenHeight = 0;
	screenWidth = 0;

	//init win api
	InitializeWindows(screenWidth, screenHeight);

	//create input obj
	m_Input = new CInput;
	if (!m_Input)
		return false;

	//init input obj
	result = m_Input->Initialize(m_hinstance, m_hwnd, screenWidth, screenHeight);
	if (!result)
	{
		MessageBox(m_hwnd, L"Could not initialize the input object.", L"Error", MB_OK);
		return false;
	}

	//create graphics obj
	m_Graphics = new CGraphics;
	if (!m_Graphics)
		return false;

	//init graphics
	result = m_Graphics->Initialize(screenWidth, screenHeight, m_hwnd);
	if (!result)
		return false;

	//create and init the fps obj
	m_Fps = new CFpsMeter;
	if (!m_Fps)
		return false;
	m_Fps->Initialize();

	//create and init cpu obj
	m_Cpu = new CCpuMeter;
	if (!m_Cpu)
		return false;
	m_Cpu->Initiaize();

	//create and init timer obj
	m_Timer = new CTimer;
	if (!m_Timer)
		return false;

	result = m_Timer->Initialize();
	if (!result)
	{
		MessageBox(m_hwnd, L"Could not initialize the Timer object.", L"Error", MB_OK);
		return false;
	}

	return true;
}

void CSystem::Shutdown()
{
	// Release the timer object.
	if (m_Timer)
	{
		delete m_Timer;
		m_Timer = 0;
	}

	// Release the cpu object.
	if (m_Cpu)
	{
		m_Cpu->Shutdown();
		delete m_Cpu;
		m_Cpu = 0;
	}

	// Release the fps object.
	if (m_Fps)
	{
		delete m_Fps;
		m_Fps = 0;
	}

	//release graphics
	if (m_Graphics)
	{
		m_Graphics->Shutdown();
		delete m_Graphics;
		m_Graphics = 0;
	}

	//release input
	if (m_Input)
	{
		m_Input->Shutdown();
		delete m_Input;
		m_Input = 0;
	}

	//shutdown windows
	ShutdownWindows();
}

void CSystem::Run()
{
	MSG msg;
	bool done, result;

	//init msg struct
	ZeroMemory(&msg, sizeof(MSG));

	//loop until quit
	done = false;

	while (!done)
	{
		//handle windows msg
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//if quit message
		if (msg.message == WM_QUIT)
			done = true;
		else
		{
			//otherwise - frame processing
			result = Frame();
			if (!result)
				done = true;
		}
		// Check if the user pressed escape and wants to quit.
		if (m_Input->IsEscapePressed())
			done = true;
	}
}

bool CSystem::Frame()
{
	bool result;

	//update fps, cpu and timer
	m_Cpu->Frame();
	m_Fps->Frame();
	m_Timer->Frame();

	int mouseX, mouseY;

	result = m_Input->Frame();

	if (!result)
		return false;

	//Get the location of the mouse
	m_Input->GetMouseLocation(mouseX, mouseY);

	result = m_Graphics->Frame(mouseX, mouseY, m_Fps->GetFps(), m_Cpu->GetCpuPercentage(), m_Timer->GetTime());
	if (!result)
		return false;

	result = m_Graphics->Render();
	if (!result)
		return false;

	return true;
}

LRESULT CALLBACK CSystem::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}

void CSystem::InitializeWindows(int &screenWidth, int &screenHeight)
{
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;

	ApplicationHandle = this;
	m_hinstance = GetModuleHandle(NULL);//get the instance of app
	m_applicationName = L"Engine";//name

	//setup window class
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_hinstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = m_applicationName;
	wc.cbSize = sizeof(WNDCLASSEX);

	//register wnd class
	RegisterClassEx(&wc);

	//determine client resolution
	screenWidth = GetSystemMetrics(SM_CXSCREEN);
	screenHeight = GetSystemMetrics(SM_CYSCREEN);

	if (FULL_SCREEN)
	{
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
		dmScreenSettings.dmPelsWidth = (unsigned long)screenWidth;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
		//change display settings
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);
		posX = posY = 0;
	}
	else
	{
		//800x600 for non full screen
		screenWidth = 800;
		screenHeight = 600;

		//place wnd to center of screen
		posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth) / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
	}

	//create window
	m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName, WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP | WS_VISIBLE, posX, posY, screenWidth, screenHeight,
		NULL, NULL, m_hinstance, NULL);

	//bring to top + focus
	ShowWindow(m_hwnd, SW_SHOW);
	UpdateWindow(m_hwnd);
	SetForegroundWindow(m_hwnd);
	SetFocus(m_hwnd);
	
	//hide cursor
	ShowCursor(false);

}

void CSystem::ShutdownWindows()
{
	//show cursor
	ShowCursor(true);

	//fix the display settings when was in fullscreen
	if (FULL_SCREEN)
		ChangeDisplaySettings(NULL, 0);

	//remove the window
	DestroyWindow(m_hwnd);
	m_hwnd = NULL;

	//remove app instance
	UnregisterClass(m_applicationName, m_hinstance);
	m_hinstance = NULL;

	//release pointer to this class
	ApplicationHandle = NULL;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	switch (umsg)
	{
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
		case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}
		//other msgs
		default:
		{
			return ApplicationHandle->MessageHandler(hwnd, umsg, wparam, lparam);
		}
	}
}
