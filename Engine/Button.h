#pragma once

#include "CoreHeader.h"

class CButton : public IWidget
{
public:
	CButton();
	~CButton();

	virtual bool Initialize() override;

	virtual void Render() override;

	virtual void Shutdown() override;

private:

};