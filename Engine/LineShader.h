#pragma once

#include "CoreHeader.h"

class CLineShader
{
public:
	CLineShader();
	CLineShader(const CLineShader &);
	~CLineShader();

	bool Initialize(ID3D10Device *, HWND);
	void Shutdown();
	void Render(ID3D10Device* device, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix);

private:
	bool InitializeShader(ID3D10Device *, HWND, WCHAR *);
	void ShutdownShader();
	void SetShaderParameters(D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix);

	void RenderShader(ID3D10Device *, int);

	void OutputShaderErrorMessage(ID3D10Blob * errorMessage, HWND hwnd, WCHAR * shaderFilename);


	ID3D10Effect* m_effect;
	ID3D10EffectTechnique* m_technique;
	ID3D10InputLayout* m_layout;

	ID3D10EffectMatrixVariable* m_pWorldMatrix;
	ID3D10EffectMatrixVariable* m_pViewMatrix;
	ID3D10EffectMatrixVariable* m_pProjectionMatrix;
	//ID3D10EffectVectorVariable *m_pLineColor;

};