#pragma once

#include "CoreHeader.h"

class CD3Device
{
public:
	CD3Device();
	CD3Device(const CD3Device &);
	~CD3Device();

	bool Initialize(int screenWidth, int screenHeight, bool, HWND hwnd, bool, float, float);
	void Shutdown();

	void BeginScene(float r, float g, float b, float a);
	void EndScene();

	ID3D10Device* GetDevice();

	void GetProjectionMatrix(D3DXMATRIX &proj);
	void GetWorldMatrix(D3DXMATRIX &world);
	void GetOrthoMatrix(D3DXMATRIX &ortho);

	void GetVideoCardInfo(char *, int &);

	void TurnZBufferOn();
	void TurnZBufferOff();

private:
	bool m_vsync;
	int m_videoCardMemory;
	char m_videoCardDescription[128];
	IDXGISwapChain* m_swapChain;
	ID3D10Device* m_device;
	ID3D10RenderTargetView* m_renderTargetView;
	ID3D10Texture2D* m_depthStencilBuffer;
	ID3D10DepthStencilState* m_depthStencilState;
	ID3D10DepthStencilView* m_depthStencilView;
	ID3D10RasterizerState* m_rasterState;
	D3DXMATRIX m_projMatrix;
	D3DXMATRIX m_worldMatrix;
	D3DXMATRIX m_orthoMatrix;

	ID3D10DepthStencilState *m_depthDisabledStencilState;

};

