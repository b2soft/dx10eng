#include "CoreHeader.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd )
{
	CSystem *system;
	bool result;

	//Create the system object

	system = new CSystem;
	
	if (!system)
		return 0;

	//Init and run system obj
	result = system->Initialize();

	if (result)
		system->Run();

	//shutdown and release
	system->Shutdown();
	delete system;
	system = 0;

	return 0;

}