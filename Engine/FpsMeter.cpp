#include "FpsMeter.h"



CFpsMeter::CFpsMeter(const CFpsMeter &)
{

}

CFpsMeter::CFpsMeter()
{

}

CFpsMeter::~CFpsMeter()
{

}

void CFpsMeter::Initialize()
{
	m_fps = 0;
	m_count = 0;
	m_startTime = timeGetTime();
}

void CFpsMeter::Frame()
{
	m_count++;
	if (timeGetTime() >= (m_startTime + 1000))
	{
		m_fps = m_count;
		m_count = 0;

		m_startTime = timeGetTime();
	}
}

int CFpsMeter::GetFps() const
{
	return m_fps;
}
