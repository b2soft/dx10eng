#include "Timer.h"



CTimer::CTimer(const CTimer &)
{

}

CTimer::CTimer()
{

}

CTimer::~CTimer()
{

}

//The Initialize function will first query the system to see if it supports high frequency timers.
//If it returns a frequency then we use that value to determine how many counter ticks will occur each millisecond.
//We can then use that value each frame to calculate the frame time.
//At the end of the Initialize function we query for the start time of this frame to start the timing.
bool CTimer::Initialize()
{
	// Check to see if this system supports high performance timers.
	QueryPerformanceFrequency((LARGE_INTEGER *)&m_frequency);
	if (m_frequency == 0)
		return false;

	// Find out how many times the frequency counter ticks every millisecond.
	m_ticksPerMs = m_frequency / 1000.0f;

	QueryPerformanceCounter((LARGE_INTEGER *)&m_startTime);
}

void CTimer::Frame()
{
	INT64 currentTime;
	float timeDifference;

	QueryPerformanceCounter((LARGE_INTEGER *)&currentTime);
	timeDifference = static_cast<float>(currentTime - m_startTime);
	m_frameTime = timeDifference / m_ticksPerMs;
	m_startTime = currentTime;
}

float CTimer::GetTime() const
{
	return m_frameTime;
}
