#include "Input.h"

CInput::CInput()
{
	m_directInput = 0;
	m_mouse = 0;
	m_keyboard = 0;
}

CInput::CInput(const CInput &)
{

}

CInput::~CInput()
{

}

bool CInput::Initialize(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight)
{
	HRESULT hr;
	// Store the screen size which will be used for positioning the mouse cursor.
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	//Initialize mouse location
	m_mouseX = 0;
	m_mouseY = 0;

	//Initialize DirectInput Device
	hr = DirectInput8Create(hinstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void **)&m_directInput, NULL);
	if (FAILED(hr))
		return false;

	//Initialize keyboard
	//Initialize direct input interface for keyboard
	hr = m_directInput->CreateDevice(GUID_SysKeyboard, &m_keyboard, NULL);
	if (FAILED(hr))
		return false;

	//set the data format. In this case since it is a keyboard we can use the predefined data format.
	hr = m_keyboard->SetDataFormat(&c_dfDIKeyboard);
	if (FAILED(hr))
		return false;

	// Set the cooperative level of the keyboard to not share with other programs.
	hr = m_keyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE);
	if (FAILED(hr))
		return false;

	//now acquire the keyboard
	hr = m_keyboard->Acquire();
	if (FAILED(hr))
		return false;

	////////////
	//Initialize device for mouse
	hr = m_directInput->CreateDevice(GUID_SysMouse, &m_mouse, NULL);
	if (FAILED(hr))
		return false;

	//set the data as mouse
	hr = m_mouse->SetDataFormat(&c_dfDIMouse);
	if (FAILED(hr))
		return false;

	//set the cooperative level of the mouse to share with other programs.
	hr = m_mouse->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if (FAILED(hr))
		return false;

	//Acquire mouse
	hr = m_mouse->Acquire();
	if (FAILED(hr))
		return false;

	return true;
}

void CInput::Shutdown()
{
	//Release mouse
	if (m_mouse)
	{
		m_mouse->Unacquire();
		m_mouse->Release();
		m_mouse = 0;
	}

	//release keyboard
	if (m_keyboard)
	{
		m_keyboard->Unacquire();
		m_keyboard->Release();
		m_keyboard = 0;
	}

	//release directinput
	if (m_directInput)
	{
		m_directInput->Release();
		m_directInput = 0;
	}
}

//The Frame function for the InputClass will read the current state of the devices into state buffers we setup.
//After the state of each device is read it then processes the changes.
bool CInput::Frame()
{
	bool result;

	// Read the current state of the keyboard.
	result = ReadKeyboard();
	if (!result)
		return false;

	// Read the current state of the mouse.
	result = ReadMouse();
	if (!result)
		return false;

	// Process the changes in the mouse and keyboard.
	ProcessInput();

	return true;
}

bool CInput::IsEscapePressed()
{
	// Do a bitwise and on the keyboard state to check if the escape key is currently being pressed.
	if (m_keyboardState[DIK_ESCAPE] & 0x80)
		return true;

	return false;
}

void CInput::GetMouseLocation(int &mouseX, int &mouseY)
{
	mouseX = m_mouseX;
	mouseY = m_mouseY;
}

//ReadKeyboard will read the state of the keyboard into the m_keyboardState variable.
//The state will show any keys that are currently pressed or not pressed.
//If it fails reading the keyboard then it can be for one of five different reasons.
//The only two that we want to recover from are if the focus is lost or if it becomes unacquired.
//If this is the case we call acquire each frame until we do get control back.
//The window may be minimized in which case Acquire will fail, but once the window comes to the foreground again then Acquire will succeed and we will be able to read the keyboard state.
bool CInput::ReadKeyboard()
{
	HRESULT hr;

	//Read the keyboard device
	hr = m_keyboard->GetDeviceState(sizeof(m_keyboardState), (LPVOID)&m_keyboardState);
	if (FAILED(hr))
	{
		// If the keyboard lost focus or was not acquired then try to get control back.
		if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
			m_keyboard->Acquire();
		else
			return false;
	}
	return true;
}

bool CInput::ReadMouse()
{
	HRESULT hr;

	//Read the mouse device
	hr = m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&m_mouseState);
	if (FAILED(hr))
	{
		// If the mouse lost focus or was not acquired then try to get control back.
		if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
			m_mouse->Acquire();
		else
			return false;
	}
	return true;
}

void CInput::ProcessInput()
{
	// Update the location of the mouse cursor based on the change of the mouse location during the frame.
	m_mouseX += m_mouseState.lX;
	m_mouseY += m_mouseState.lY;

	//Ensure mouse doesn't go over screen width/height
	if (m_mouseX < 0) { m_mouseX = 0; }
	if (m_mouseY < 0) { m_mouseY = 0; }

	if (m_mouseX > m_screenWidth) { m_mouseX = m_screenWidth; }
	if (m_mouseY > m_screenHeight) { m_mouseY = m_screenHeight; }
}
