#pragma once

#include "CoreHeader.h"

class CD3Device;
class CTimer;
class CCpuMeter;
class CFpsMeter;
class CCamera;
class CModel;
class CLight;
class CLightShader;
class CBitmap;
class CTextureShader;
class CText;
class CLineShader;
class CLine;
class CPlot;

const bool FULL_SCREEN = false;
const bool VSYNC = false;
const float SCREEN_DEPTH = 1000.0f;
const float SCREEN_NEAR = 0.1f;

class CGraphics
{
public:
	CGraphics();
	CGraphics(const CGraphics &);
	~CGraphics();

	bool Initialize(int, int, HWND);
	void Shutdown();
	bool Frame(int mouseX, int mouseY, int fps, int cpu, float frameTime);
	bool Render();


	CPlot &GetPlot() const;
private:
	CD3Device *m_D3D;
	CCamera *m_Camera;
	CModel *m_Model;
	CLightShader *m_LightShader;
	CLight *m_Light;

	CTextureShader *m_TextureShader;

	CBitmap *m_Bitmap;
	CText *m_Text;
	


	CLineShader *m_LineShader;
	CLine *m_Line;
	CPlot* m_Plot;
};
