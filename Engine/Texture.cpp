#include "Texture.h"

CTexture::CTexture(const CTexture &)
{

}

CTexture::CTexture()
{
	m_texture = 0;
}

CTexture::~CTexture()
{
}

bool CTexture::Initialize(ID3D10Device *device, WCHAR *filename)
{
	HRESULT hr;

	//load texture
	hr = D3DX10CreateShaderResourceViewFromFile(device, filename, NULL, NULL, &m_texture, NULL);
	if (FAILED(hr))
		return false;

	return true;
}

void CTexture::Shutdown()
{
	if (m_texture)
	{
		m_texture->Release();
		m_texture = 0;
	}
}

ID3D10ShaderResourceView * CTexture::GetTexture()
{
	return m_texture;
}
