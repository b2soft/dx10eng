#pragma once

#include "CoreHeader.h"

class CLightShader
{
public:
	CLightShader();
	CLightShader(const CLightShader &);
	~CLightShader();

	bool Initialize(ID3D10Device *, HWND);
	void Shutdown();
	void Render(ID3D10Device *, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D10ShaderResourceView *, D3DXVECTOR3, D3DXVECTOR4, D3DXVECTOR4, D3DXVECTOR3, D3DXVECTOR4, float);

private:
	bool InitalizeShader(ID3D10Device *, HWND, WCHAR *);
	void ShutdownShader();
	void OutputShaderErrorMsg(ID3D10Blob *, HWND, WCHAR *);

	void SetShaderParametres(D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D10ShaderResourceView *, D3DXVECTOR3, D3DXVECTOR4, D3DXVECTOR4, D3DXVECTOR3, D3DXVECTOR4, float);
	void RenderShader(ID3D10Device *, int);

	ID3D10Effect *m_effect;
	ID3D10EffectTechnique *m_technique;
	ID3D10InputLayout *m_layout;

	ID3D10EffectMatrixVariable *m_pWorldMatrix;
	ID3D10EffectMatrixVariable *m_pViewMatrix;
	ID3D10EffectMatrixVariable *m_pProjMatrix;

	ID3D10EffectShaderResourceVariable *m_pTexture;

	ID3D10EffectVectorVariable *m_pLightDirection;
	ID3D10EffectVectorVariable *m_pDiffuseColor;
	ID3D10EffectVectorVariable *m_pAmbientColor;

	ID3D10EffectVectorVariable *m_pCameraPosition;
	ID3D10EffectVectorVariable *m_pSpecularColor;
	ID3D10EffectScalarVariable *m_pSpecularPower;
};

