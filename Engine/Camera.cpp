#include "Camera.h"

CCamera::CCamera()
{
	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
}

CCamera::CCamera(const CCamera &)
{

}

CCamera::~CCamera()
{

}

void CCamera::SetPosition(float x, float y, float z)
{
	m_pos.x = x;
	m_pos.y = y;
	m_pos.z = z;
}

void CCamera::SetPosition(D3DXVECTOR3 &pos)
{
	m_pos = pos;
}

void CCamera::SetRotation(float x, float y, float z)
{
	m_rotation.x = x;
	m_rotation.y = y;
	m_rotation.z = z;
}

void CCamera::SetRotaton(D3DXVECTOR3 &rot)
{
	m_rotation = rot;
}

D3DXVECTOR3 CCamera::GetPosition()
{
	return m_pos;
}

D3DXVECTOR3 CCamera::GetRotation()
{
	return m_rotation;
}

void CCamera::Render()
{
	D3DXVECTOR3 up, position, lookAt;
	float pitch, yaw, roll;
	D3DXMATRIX rotationMatrix;

	//upwards vector
	up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	//setup camera pos in world
	position = m_pos;

	//setup where it looks
	lookAt = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = m_rotation.x * D3DX_PI / 180.0f;
	yaw = m_rotation.y * D3DX_PI / 180.0f;
	roll = m_rotation.z * D3DX_PI / 180.0f;

	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);

	// Translate the rotated camera position to the location of the viewer.
	lookAt += position;

	// Finally create the view matrix from the three updated vectors.
	D3DXMatrixLookAtLH(&m_viewMatrix, &position, &lookAt, &up);
}

void CCamera::GetViewMatrix(D3DXMATRIX &viewMatrix)
{
	viewMatrix = m_viewMatrix;
}
