#pragma once

#include "CoreHeader.h"

class CLineShader;

class CLine
{
public:
	CLine();
	CLine(const CLine &);
	~CLine();

	bool Initialize(ID3D10Device * device, D3DXVECTOR3 f1, D3DXVECTOR3 f2, D3DXVECTOR4 color);
	void Shutdown();
	bool Render(ID3D10Device *);

private:
	bool InitializeBuffers(ID3D10Device *device);
	void ShutdownBuffers();
	void RenderBuffers(ID3D10Device *device);

	ID3D10Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;

	D3DXVECTOR3 m_fix1, m_fix2;
	D3DXVECTOR4 m_color;
	
	//CLineShader

	struct SVertexType //must match shader type!
	{
		D3DXVECTOR3 position;
		D3DXVECTOR4 color;
	};
};