#pragma once

#include "CoreHeader.h"

class CFont;
class CFontShader;

class CText
{
private:
	struct SSentenceType
	{
		ID3D10Buffer *vertexBuffer, *indexBuffer;
		int vertexCount, indexCount, maxLength;
		float red, green, blue;
	};

	struct SVertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};


public:
	CText();
	CText(const CText&);
	~CText();

	bool Initialize(ID3D10Device *, HWND, int, int, D3DXMATRIX);
	void Shutdown();
	void Render(ID3D10Device *, D3DXMATRIX, D3DXMATRIX);

	bool SetMousePosition(int, int);
	bool SetFps(int fps);
	bool SetCpu(int cpu);

private:
	bool InitializeSentence(SSentenceType **, int, ID3D10Device *);
	bool UpdateSentence(SSentenceType *, char *, int, int, float, float, float);
	void ReleaseSentence(SSentenceType** sentence);
	void RenderSentence(ID3D10Device *, SSentenceType *, D3DXMATRIX, D3DXMATRIX);

	CFont *m_Font;
	CFontShader *m_FontShader;
	int m_screenWidth, m_screenHeight;
	D3DXMATRIX m_baseViewMatrix;

	SSentenceType *m_sentence1;
	SSentenceType *m_sentence2;
	SSentenceType *m_fpsSentence;
	SSentenceType *m_cpuSentence;
};