#pragma once

#include "CoreHeader.h"

class IWidget
{
public:
	IWidget();
	virtual ~IWidget();

	virtual bool Initialize() = 0;
	virtual void Render() = 0;
	virtual void Shutdown() = 0;

private:

};