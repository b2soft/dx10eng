#include "Graphics.h"
#include <cstdio>

CLineShader *shader;

CGraphics::CGraphics()
{
	m_D3D = 0;
	m_Camera = 0;
	m_Model = 0;
	m_LightShader = 0;
	m_Light = 0;
	m_TextureShader = 0;
	m_Bitmap = 0;
	m_Text = 0;
	m_Plot = 0;
}

CGraphics::~CGraphics()
{

}

CGraphics::CGraphics(const CGraphics &)
{

}

bool CGraphics::Initialize(int screenWidth, int screenHeight, HWND hwnd)
{
	bool result;
	D3DXMATRIX baseViewMatrix;

	//create Direct3D object
	m_D3D = new CD3Device();
	if (!m_D3D)
		return false;

	//initialize D3D
	result = m_D3D->Initialize(screenWidth, screenHeight, VSYNC, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize Direct3D", L"Error", MB_OK);
		return false;
	}

	//create + init camera obj
	m_Camera = new CCamera;
	if (!m_Camera) //-V668
		return false;

	//set initial position
	m_Camera->SetPosition(0.0f, 0.0f, -10.0f);

	// Create the texture shader object.
	m_TextureShader = new CTextureShader;
	if (!m_TextureShader)
		return false;

	// Initialize the texture shader object.
	result = m_TextureShader->Initialize(m_D3D->GetDevice(), hwnd);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the texture shader object.", L"Error", MB_OK);
		return false;
	}

	// Create the bitmap object.
	m_Bitmap = new CBitmap;
	if (!m_Bitmap)
	{
		return false;
	}

	

	// Initialize the bitmap object.
	result = m_Bitmap->Initialize(m_D3D->GetDevice(), screenWidth, screenHeight, L"Resources/seafloor2.dds", 128, 128);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the bitmap object.", L"Error", MB_OK);
		return false;
	}

	// Initialize a base view matrix with the camera for 2D user interface rendering.
	m_Camera->SetPosition(0.0f, 0.0f, -1.0f);
	m_Camera->Render();
	m_Camera->GetViewMatrix(baseViewMatrix);

	// Create the text object.
	m_Text = new CText;
	if (!m_Text)
		return false;

	// Initialize the text object.
	result = m_Text->Initialize(m_D3D->GetDevice(), hwnd, screenWidth, screenHeight, baseViewMatrix);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the text object.", L"Error", MB_OK);
		return false;
	}

/*	m_Line = new CLine();
	m_Line->Initialize(m_D3D->GetDevice(), D3DXVECTOR3(-1.0f, 1.0f, 0.0f), D3DXVECTOR3(1.0f, -1.0f, 0.0f), D3DXVECTOR4(0.0f, 1.0f, 0.0f, 0.0f));

	m_LineShader = new CLineShader();
	if (!m_LineShader)
		return false;

	result = m_LineShader->Initialize(m_D3D->GetDevice(), hwnd);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the line object.", L"Error", MB_OK);
		return false;
	}*/

	m_Plot = new CPlot();
	m_Plot->Initialize(m_D3D->GetDevice(), hwnd, D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(10.0f, 10.0f, 10.0f), 3.0);


	/*
	//create and init model obj
	m_Model = new CModel;
	if (!m_Model)
		return false;

	result = m_Model->Initialize(m_D3D->GetDevice(), "Resources/cube.txt",  L"Resources/seafloor.dds");
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the model object.", L"Error", MB_OK);
		return false;
	}

	//create and init colorshader obj
	m_LightShader = new CLightShader;
	if (!m_LightShader)
		return false;

	result = m_LightShader->Initialize(m_D3D->GetDevice(), hwnd);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the light shader object.", L"Error", MB_OK);
		return false;
	}

	//create light obj

	m_Light = new CLight;
	if (!m_Light)
		return false;

	m_Light->SetAmbientColor(0.15f, 0.15f, 0.15f, 1.0f);
	m_Light->SetDiffuseColor(1.0f, 1.0f, 1.0f, 1.0f);
	m_Light->SetDirection(0.0f, 0.0f, 1.0f);
	m_Light->SetSpecularColor(1.0f, 1.0f, 1.0f, 1.0f);
	m_Light->SetSpecularPower(32.0f);*/

	return true;
}

bool CGraphics::Frame(int mouseX, int mouseY, int fps, int cpu, float frameTime)
{
	bool result;

	// Set the location of the mouse.
	result = m_Text->SetMousePosition(mouseX, mouseY);
	if (!result)
		return false;
	
	result = m_Text->SetFps(fps);
	if (!result)
		return false;

	result = m_Text->SetCpu(cpu);
	if (!result)
		return false;


	// Set the position of the camera.
	//m_Camera->SetPosition(0.0f, 0.0f, -10.0f);
	m_Camera->SetPosition(0.0f, 0.0f, -30.0f);

	return true;
}

void CGraphics::Shutdown()
{
	if (m_Plot)
	{
		m_Plot->Shutdown();
		delete m_Plot;
		m_Plot = 0;
	}

	// Release the text object.
	/*if (m_LineShader)
	{
		m_LineShader->Shutdown();
		delete m_LineShader;
		m_LineShader = 0;
	}*/

	if (m_Text)
	{
		m_Text->Shutdown();
		delete m_Text;
		m_Text = 0;
	}

	// Release the bitmap object.
	if (m_Bitmap)
	{
		m_Bitmap->Shutdown();
		delete m_Bitmap;
		m_Bitmap = 0;
	}

	// Release the texture shader object.
	if (m_TextureShader)
	{
		m_TextureShader->Shutdown();
		delete m_TextureShader;
		m_TextureShader = 0;
	}

	/*if (m_Light)
	{
		delete m_Light;
		m_Light = 0;
	}

	//release all pointers
	if (m_LightShader)
	{
		m_LightShader->Shutdown();
		delete m_LightShader;
		m_LightShader = 0;
	}

	if (m_Model)
	{
		m_Model->Shutdown();
		delete m_Model;
		m_Model = 0;
	}*/

	if (m_Camera)
	{
		delete m_Camera;
		m_Camera = 0;
	}

	if (m_D3D)
	{
		m_D3D->Shutdown();
		delete m_D3D;
		m_D3D = 0;
	}
}

bool CGraphics::Render()
{
	D3DXMATRIX viewMatrix, projectionMatrix, worldMatrix, orthoMatrix;
	bool result;

	//clear buffers to begin the scene (BLACK)
	m_D3D->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

	m_Camera->Render();

	m_Camera->GetViewMatrix(viewMatrix);
	m_D3D->GetWorldMatrix(worldMatrix);
	m_D3D->GetProjectionMatrix(projectionMatrix);

	m_D3D->GetOrthoMatrix(orthoMatrix);

	// Turn off the Z buffer to begin all 2D rendering.
	m_D3D->TurnZBufferOff();

	// Put the bitmap vertex and index buffers on the graphics pipeline to prepare them for drawing.
/*	result = m_Bitmap->Render(m_D3D->GetDevice(), 100, 100);
	if (!result)
		return false;

	// Render the bitmap using the texture shader.
	m_TextureShader->Render(m_D3D->GetDevice(), m_Bitmap->GetIndexCount(), worldMatrix, viewMatrix, orthoMatrix, m_Bitmap->GetTexture());*/
	// Render the text strings.
	m_Text->Render(m_D3D->GetDevice(), worldMatrix, orthoMatrix);

	m_D3D->TurnZBufferOn();

	//m_Line->Render(m_D3D->GetDevice());
	
	
	//RenderLine(worldMatrix, viewMatrix, projectionMatrix);


	//m_LineShader->Render(m_D3D->GetDevice(), 2, worldMatrix, viewMatrix, projectionMatrix);
	m_Plot->Render(worldMatrix, viewMatrix, projectionMatrix);
//	RenderLine(worldMatrix, viewMatrix, projectionMatrix);

	/*D3DXMatrixRotationY(&worldMatrix, rotation);

	m_Model->Render(m_D3D->GetDevice());

	// Render the model using the light shader.
	m_LightShader->Render(m_D3D->GetDevice(), m_Model->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix, m_Model->GetTexture(),
		m_Light->GetDirection(), m_Light->GetAmbientColor(), m_Light->GetDiffuseColor(), m_Camera->GetPosition(),
		m_Light->GetSpecularColor(), m_Light->GetSpecularPower());*/

	m_D3D->EndScene();

	return true;
}

CPlot &CGraphics::GetPlot() const
{
	return *m_Plot;
}

