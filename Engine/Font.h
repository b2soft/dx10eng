#pragma once

#include "CoreHeader.h"

class CTexture;

class CFont
{
public:
	CFont();
	CFont(const CFont &);
	~CFont();

	bool Initialize(ID3D10Device *, char *, WCHAR *);
	void Shutdown();

	ID3D10ShaderResourceView *GetTexture();
	void BuildVertexArray(void *, char *, float, float);

private:
	bool LoadFontData(char *);
	void ReleaseFontData();
	bool LoadTexture(ID3D10Device *, WCHAR *);
	void ReleaseTexture();


	struct SFontType 
	{
		float left, right;
		int size;
	};

	struct SVertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};

	SFontType *m_Font;
	CTexture *m_Texture;
};
