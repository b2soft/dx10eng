#pragma once

#include "CoreHeader.h"

class CTexture;

class CModel
{
public:
	CModel();
	CModel(const CModel &);
	~CModel();

	bool Initialize(ID3D10Device *, char *, WCHAR *);
	void Shutdown();
	void Render(ID3D10Device *);//The Render function puts the model geometry on the video card to prepare it for drawing by the color shader

	int GetIndexCount() const;

	bool LoadModel(char *);
	void ReleaseModel();

	ID3D10ShaderResourceView *GetTexture();

private:
	bool InitializeBuffers(ID3D10Device *);
	void ShutdownBuffers();
	void RenderBuffers(ID3D10Device *);

	ID3D10Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;

	bool LoadTexture(ID3D10Device *, WCHAR *);
	void ReleaseTexture();

	CTexture *m_texture;

	struct SVertexType //must match shader type!
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
		D3DXVECTOR3 normal;
	};

	struct SModelType
	{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};

	SModelType *m_model;
};

