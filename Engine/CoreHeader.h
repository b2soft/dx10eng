#pragma once

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <mmsystem.h>
#include <fstream>
#include <pdh.h>

using namespace std;

#pragma warning (disable: 4005) //using old HEADERS!!!! Win 8 FUCK!

#define DIRECTINPUT_VERSION 0x0800

#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "pdh.lib")

#pragma comment(lib, "d3d10.lib")
#ifdef _DEBUG
#pragma comment(lib, "d3dx10d.lib")
#else
#pragma comment(lib, "d3dx10.lib")
#endif

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

#include <d3dx10.h>
#include <D3DX10math.h>
#include <dinput.h>

#include "Input.h"
#include "FpsMeter.h"
#include "CpuMeter.h"
#include "Timer.h"
#include "LightShader.h"
#include "Texture.h"
#include "TextureShader.h"
#include "Font.h"
#include "FontShader.h"
#include "Text.h"
#include "LineShader.h"
#include "Line.h"
#include "Plot.h"
//here GUI
#include "IWidget.h"
#include "Button.h"
//end GUI
#include "Model.h"
#include "Light.h"
#include "Bitmap.h"
#include "Camera.h"
#include "D3Device.h"
#include "Graphics.h"
#include "System.h"




