#include "Line.h"



CLine::~CLine()
{

}

bool CLine::Initialize(ID3D10Device * device, D3DXVECTOR3 f1, D3DXVECTOR3 f2, D3DXVECTOR4 color)
{
	bool result;

	m_fix1 = f1;
	m_fix2 = f2;
	
	m_color = color;

	result = InitializeBuffers(device);
	return result;
}

void CLine::Shutdown()
{
	ShutdownBuffers();
}

bool CLine::Render(ID3D10Device *device)
{
	RenderBuffers(device);
	return true;
}

bool CLine::InitializeBuffers(ID3D10Device *device)
{
	SVertexType *vertices;
	unsigned long *indices;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT hr;

	m_vertexCount = m_indexCount = 2;

	vertices = new SVertexType[m_vertexCount];
	if (!vertices)
		return false;

	indices = new unsigned long[m_indexCount];
	if (!indices)
		return false;

	memset(vertices, 0, (sizeof(SVertexType) * m_vertexCount));

	for (int i = 0; i < m_indexCount; i++)
		indices[i] = i;

	vertexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(SVertexType) * m_vertexCount; //total size of buffer in bytes
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	
	vertexData.pSysMem = vertices;

	SVertexType v[] = { {m_fix1, m_color}, {m_fix2, m_color} };

	vertexData.pSysMem = v;

	device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);

	//set up index buf desc
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * 2;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	indexData.pSysMem = indices;

	device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);

	return true;
}

void CLine::ShutdownBuffers()
{
	//release the buffers
	if (m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	if (m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}
}

void CLine::RenderBuffers(ID3D10Device *device)
{
	UINT stride = sizeof(SVertexType);
	UINT offset = 0;
	device->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);
	device->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINELIST);
}

CLine::CLine()
{
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
}

CLine::CLine(const CLine &)
{

}
