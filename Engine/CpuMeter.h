#pragma once

#include "CoreHeader.h"

class CCpuMeter
{
public:
	CCpuMeter();
	CCpuMeter(const CCpuMeter &);
	~CCpuMeter();

	void Initiaize();
	void Shutdown();
	void Frame();

	int GetCpuPercentage() const;

private:
	bool m_canReadCpu;
	HQUERY m_queryHandle;
	HCOUNTER m_counterHandle;
	unsigned long m_lastSampleTime;
	long m_cpuUsage;

};
