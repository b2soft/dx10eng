#pragma once

#include "CoreHeader.h"

class CTexture
{
public:
	CTexture();
	CTexture(const CTexture &);
	~CTexture();

	bool Initialize(ID3D10Device *device, WCHAR *filename);
	void Shutdown();

	ID3D10ShaderResourceView *GetTexture();

private:
	ID3D10ShaderResourceView *m_texture;

};

