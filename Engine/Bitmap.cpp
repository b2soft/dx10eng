#include "Bitmap.h"

CBitmap::CBitmap()
{
	m_vertexBuffer = 0;
	m_indexBuffer = 0;
	m_texture = 0;
}

CBitmap::CBitmap(const CBitmap &)
{

}

CBitmap::~CBitmap()
{
}

bool CBitmap::Initialize(ID3D10Device *device, int screenWidth, int screenHeight, WCHAR* textureFilename, int bitmapWidth, int bitmapHeight)
{
	bool result;

	// Store the screen size.
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	// Store the size in pixels that this bitmap should be rendered at.
	m_bitmapWidth = bitmapWidth;
	m_bitmapHeight = bitmapHeight;

	// Initialize the previous rendering position to negative one.
	m_prevPosX = -1;
	m_prevPosY = -1;

	result = InitializeBuffers(device);// Initialize the vertex and index buffer that hold the geometry for triangles
	if (!result)
		return false;

	result = LoadTexture(device, textureFilename);
	if (!result)
		return false;

	return true;
}

void CBitmap::Shutdown()
{
	ReleaseTexture();
	ShutdownBuffers();
}

bool CBitmap::Render(ID3D10Device* device, int posX, int posY)
{
	bool result;

	// Re-build the dynamic vertex buffer for rendering to possibly a different location on the screen.
	result = UpdateBuffers(posX, posY);
	if (!result)
		return false;

	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(device);

	return true;
}

int CBitmap::GetIndexCount() const
{
	return m_indexCount;
}

bool CBitmap::InitializeBuffers(ID3D10Device *device)
{
	SVertexType *vertices;
	unsigned long *indices;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT hr;

	m_vertexCount = 6;

	m_indexCount = m_vertexCount;

	//create vertex arr
	vertices = new SVertexType[m_vertexCount];
	if (!vertices)
		return false;

	indices = new unsigned long[m_indexCount];
	if (!indices)
		return false;

	memset(vertices, 0, (sizeof(SVertexType) * m_vertexCount)); 

	//load dat to vertex+index arrays
	for (int i = 0; i < m_indexCount; i++)
		indices[i] = i;

	//set up vertex buf desc
	vertexBufferDesc.Usage = D3D10_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(SVertexType) * m_vertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;

	//create vertex buffer
	hr = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if (FAILED(hr))
		return false;

	//set up index buf desc
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;

	//create index buffer	
	hr = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if (FAILED(hr))
		return false;

	//after creating buffers - we not need them as arrays, delete it!
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;

	return true;
}

void CBitmap::ShutdownBuffers()
{
	//release the buffers
	if (m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = 0;
	}

	if (m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = 0;
	}
}

bool CBitmap::UpdateBuffers(int posX, int posY)
{
	float left, right, top, bottom;
	SVertexType *vertices;
	void *pVertices;
	HRESULT hr;

	if ((posX == m_prevPosX) && (posY == m_prevPosY))
		return true;

	m_prevPosX = posX;
	m_prevPosY = posY;

	left = (float)((m_screenWidth / 2) * -1) + (float)posX;
	right = left + (float)m_bitmapWidth;
	top = (float)(m_screenHeight / 2) - (float)posY;
	bottom = top - (float)m_bitmapHeight;

	//Create vertex array
	vertices = new SVertexType[m_vertexCount];
	if (!vertices)
		return false;

	//Load data to triangles
	//First one
	vertices[0].position = D3DXVECTOR3(left, top, 0.0f); //top left
	vertices[0].texture = D3DXVECTOR2(0.0f, 0.0f);
	
	vertices[1].position = D3DXVECTOR3(right, bottom, 0.0f); //bottom right
	vertices[1].texture = D3DXVECTOR2(1.0f, 1.0f);

	vertices[2].position = D3DXVECTOR3(left, bottom, 0.0f); //bottom left
	vertices[2].texture = D3DXVECTOR2(0.0f, 1.0f);

	//Second one
	vertices[3].position = D3DXVECTOR3(left, top, 0.0f); //top left
	vertices[3].texture = D3DXVECTOR2(0.0f, 0.0f);

	vertices[4].position = D3DXVECTOR3(right, top, 0.0f); //top right
	vertices[4].texture = D3DXVECTOR2(1.0f, 0.0f);

	vertices[5].position = D3DXVECTOR3(right, bottom, 0.0f); //bottom right
	vertices[5].texture = D3DXVECTOR2(1.0f, 1.0f);

	// Initialize the vertex buffer pointer to null first.
	pVertices = 0;

	// Lock the vertex buffer.
	hr = m_vertexBuffer->Map(D3D10_MAP_WRITE_DISCARD, 0, (void **)&pVertices);
	if (FAILED(hr))
		return false;

	// Copy the data into the vertex buffer.
	memcpy(pVertices, (void *)vertices, (sizeof(SVertexType) * m_vertexCount));

	//Unlock vertex buffer
	m_vertexBuffer->Unmap();

	//release vertex buffer
	delete[] vertices;
	vertices = 0;

	return true;
}


void CBitmap::RenderBuffers(ID3D10Device *device)
{
	//Purpose of function: set the vertex buffer and index buffer as active on the input assembler in the GPU
	unsigned int stride;
	unsigned int offset;

	//set stride and offset
	stride = sizeof(SVertexType);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	device->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	//set primitives in vertex buffer as triangles list
	device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

ID3D10ShaderResourceView *CBitmap::GetTexture()
{
	return m_texture->GetTexture();
}

bool CBitmap::LoadTexture(ID3D10Device *device, WCHAR *filename)
{
	bool result;

	//create texture obj
	m_texture = new CTexture;
	if (!m_texture)
		return false;

	//init obj texture
	result = m_texture->Initialize(device, filename);
	if (!result)
		return false;

	return true;
}

void CBitmap::ReleaseTexture()
{
	if (m_texture)
	{
		m_texture->Shutdown();
		delete m_texture;
		m_texture = 0;
	}
}