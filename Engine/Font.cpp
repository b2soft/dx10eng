#include "Font.h"
CFont::CFont()
{
	m_Font = 0;
	m_Texture = 0;
}


bool CFont::Initialize(ID3D10Device *device, char *fontFilename, WCHAR *textureFilename)
{
	bool result;

	//load font data from file
	result = LoadFontData(fontFilename);
	if (!result)
		return false;

	// Load the texture that has the font characters on it.
	result = LoadTexture(device, textureFilename);
	if (!result)
		return false;

	return true;
}

void CFont::Shutdown()
{
	ReleaseTexture();
	ReleaseFontData();
}

ID3D10ShaderResourceView * CFont::GetTexture()
{
	return m_Texture->GetTexture();
}

void CFont::BuildVertexArray(void *vertices, char *sentence, float drawX, float drawY)
{
	SVertexType *pVertex;
	int numLetters, index, i, letter;

	// Coerce the input vertices into a VertexType structure.
	pVertex = (SVertexType *)vertices;

	//get number of letters
	numLetters = (int)strlen(sentence);

	//init index zero
	index = 0;

	//draw each letter onto quad
	for (i = 0; i < numLetters; i++)
	{
		letter = ((int)sentence[i] - 32);
		//if letter is a space - simply space 3 pixels;
		if (letter == 0)
			drawX = drawX + 3.0f;
		else
		{
			//First triangle in quad
			pVertex[index].position = D3DXVECTOR3(drawX, drawY, 0.0f);  // Top left.
			pVertex[index].texture = D3DXVECTOR2(m_Font[letter].left, 0.0f);
			index++;

			pVertex[index].position = D3DXVECTOR3((drawX + m_Font[letter].size), (drawY - 16), 0.0f);  // Bottom right.
			pVertex[index].texture = D3DXVECTOR2(m_Font[letter].right, 1.0f);
			index++;

			pVertex[index].position = D3DXVECTOR3(drawX, (drawY - 16), 0.0f);  // Bottom left.
			pVertex[index].texture = D3DXVECTOR2(m_Font[letter].left, 1.0f);
			index++;

			// Second triangle in quad.
			pVertex[index].position = D3DXVECTOR3(drawX, drawY, 0.0f);  // Top left.
			pVertex[index].texture = D3DXVECTOR2(m_Font[letter].left, 0.0f);
			index++;

			pVertex[index].position = D3DXVECTOR3(drawX + m_Font[letter].size, drawY, 0.0f);  // Top right.
			pVertex[index].texture = D3DXVECTOR2(m_Font[letter].right, 0.0f);
			index++;

			pVertex[index].position = D3DXVECTOR3((drawX + m_Font[letter].size), (drawY - 16), 0.0f);  // Bottom right.
			pVertex[index].texture = D3DXVECTOR2(m_Font[letter].right, 1.0f);
			index++;

			// Update the x location for drawing by the size of the letter and one pixel.
			drawX = drawX + m_Font[letter].size + 1.0f;
		}

	}
}

bool CFont::LoadFontData(char *filename)
{
	ifstream fin;
	int i;
	char temp;
	
	// Create the font spacing buffer.
	m_Font = new SFontType[95];
	if (!m_Font)
		return false;

	// Read in the font size and spacing between chars.
	fin.open(filename);
	if (fin.fail())
		return false;

	// Read in the 95 used ascii characters for text.
	for (i = 0; i < 95; i++)
	{
		fin.get(temp);
		while (temp != ' ')
			fin.get(temp);

		fin.get(temp);
		while (temp != ' ')
			fin.get(temp);

		fin >> m_Font[i].left;
		fin >> m_Font[i].right;
		fin >> m_Font[i].size;
	}

	// Close the file.
	fin.close();

	return true;
}

void CFont::ReleaseFontData()
{
	if (m_Font)
	{
		delete[] m_Font;
		m_Font = 0;
	}
}

bool CFont::LoadTexture(ID3D10Device *device, WCHAR *filename)
{
	bool result;

	//create texture object
	m_Texture = new CTexture;
	if (!m_Texture)
		return false;

	//Initialize texture object
	result = m_Texture->Initialize(device, filename);
	if (!result)
		return false;

	return true;
}

void CFont::ReleaseTexture()
{
	if (m_Texture)
	{
		m_Texture->Shutdown();
		delete m_Texture;
		m_Texture = 0;
	}
}

CFont::CFont(const CFont &)
{

}

CFont::~CFont()
{
}
