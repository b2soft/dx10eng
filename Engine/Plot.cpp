#include "Plot.h"



CPlot::~CPlot()
{

}

static double func(double x)
{
	return cos(x*x) / (x + 1);
}

bool CPlot::Initialize(ID3D10Device *device, HWND hwnd, D3DXVECTOR3 pos, D3DXVECTOR3 size, double scale)
{
	m_device = device;
	m_hwnd = hwnd;
	m_pos = pos;
	m_size = size;
	m_scale = scale;
	m_lines = new unordered_map<string, CLine>();
	
	m_lineShader = new CLineShader();
	if (!m_lineShader->Initialize(device, hwnd))
		return false;

	AddLine("BorderLeft", D3DXVECTOR3(-m_size.x, -m_size.y, 0.0f), D3DXVECTOR3(-m_size.x, m_size.y, 0.0f), D3DXVECTOR4(1.0f, 1.0f, 1.0f, 0.0f), true);
	AddLine("BorderRight", D3DXVECTOR3(m_size.x, -m_size.y, 0.0f), D3DXVECTOR3(m_size.x, m_size.y, 0.0f), D3DXVECTOR4(1.0f, 1.0f, 1.0f, 0.0f), true);
	AddLine("BorderUp", D3DXVECTOR3(-m_size.x, -m_size.y, 0.0f), D3DXVECTOR3(m_size.x, -m_size.y, 0.0f), D3DXVECTOR4(1.0f, 1.0f, 1.0f, 0.0f), true);
	AddLine("BorderDown", D3DXVECTOR3(-m_size.x, m_size.y, 0.0f), D3DXVECTOR3(m_size.x, m_size.y, 0.0f), D3DXVECTOR4(1.0f, 1.0f, 1.0f, 0.0f), true);

	AddLine("Ox", D3DXVECTOR3(-m_size.x, 0.0f, 0.0f), D3DXVECTOR3(m_size.x, 0.0f, 0.0f), D3DXVECTOR4(0.0f, 0.0f, 1.0f, 0.0f), true);
	AddLine("Oy", D3DXVECTOR3(0.0f, -m_size.y, 0.0f), D3DXVECTOR3(0.0f, m_size.y, 0.0f), D3DXVECTOR4(0.0f, 0.0f, 1.0f, 0.0f), true);

	AddMarks();

	BuildFunc(0.0, 2.0, func);

/*	AddLine("test", D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f), D3DXVECTOR4(1.0f, 0.0f, 0.0f, 0.0f));
	AddLine("test2", D3DXVECTOR3(1.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f), D3DXVECTOR4(1.0f, 0.0f, 0.0f, 0.0f));*/
	return true;
}

void CPlot::Shutdown()
{
	if (m_lines)
	{
		for (auto it = m_lines->begin(); it != m_lines->end(); ++it)
			(*it).second.Shutdown();

		m_lines->clear();
		delete m_lines;
		m_lines = 0;

	}
	
	if (m_lineShader)
	{
		m_lineShader->Shutdown();
		delete m_lineShader;
		m_lineShader = 0;
	}
}

void CPlot::Render(D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix)
{
	for (auto it = m_lines->begin(); it != m_lines->end(); ++it)
	{
		(*it).second.Render(m_device);
		m_lineShader->Render(m_device, m_lines->size()*2, worldMatrix, viewMatrix, projectionMatrix);
	}
	
}

void CPlot::AddLine(string name, D3DXVECTOR3 f1, D3DXVECTOR3 f2, D3DXVECTOR4 color, bool base)
{
	double scale = m_scale;
	if (base)
	{
		scale = m_size.x;
	}
	D3DXVECTOR3 f1_p;
	f1_p.x = m_pos.x + f1.x*(m_size.x / scale);
	f1_p.y = m_pos.y + f1.y*(m_size.y / scale);
	f1_p.z = 0.0f; //Z = 0

	D3DXVECTOR3 f2_p;
	f2_p.x = m_pos.x + f2.x*(m_size.x / scale);
	f2_p.y = m_pos.y + f2.y*(m_size.y / scale);
	f2_p.z = 0.0f; //Z = 0

	m_lines->insert(pair<string, CLine>(name, CLine()));
	m_lines->find(name)->second.Initialize(m_device, f1_p, f2_p, color);
}



void CPlot::AddMarks()
{
	for (int i = -m_scale; i < m_scale; ++i)
	{
		AddLine("MarkX" + to_string(i), D3DXVECTOR3(i, m_pos.y-0.1, m_pos.z), D3DXVECTOR3(i, m_pos.y + 0.1, m_pos.z), D3DXVECTOR4(0.0f, 0.0f, 1.0f, 0.0));
		AddLine("MarkY" + to_string(i), D3DXVECTOR3(m_pos.x - 0.1, i, m_pos.z), D3DXVECTOR3(m_pos.x + 0.1, i, m_pos.z), D3DXVECTOR4(0.0f, 0.0f, 1.0f, 0.0));
	}
}

void CPlot::BuildFunc(double a, double b, double(*f)(double))
{
	const int h = 500;
	double step = (b - a) / h;
	D3DXVECTOR4 color = { 1.0f, 0.0f, 0.0f, 0.0f };
	
	D3DXVECTOR3 prevCoord, newCoord;

	prevCoord.x = a;
	prevCoord.y = f(prevCoord.x);

	for (int i = 0; i < h; ++i)
	{
		newCoord.x = a + i*step;
		newCoord.y = f(newCoord.x);
		AddLine("Coord" + to_string(i), prevCoord, newCoord, color);
		prevCoord = newCoord;
	}

}

CPlot::CPlot()
{
	m_device = 0;
	m_lineShader = 0;
	m_lines = 0;
}

CPlot::CPlot(const CPlot &)
{
}
