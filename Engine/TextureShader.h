#pragma once

#include "CoreHeader.h"

class CTextureShader
{
public:
	CTextureShader();
	CTextureShader(const CTextureShader&);
	~CTextureShader();

	bool Initialize(ID3D10Device*, HWND);
	void Shutdown();
	void Render(ID3D10Device*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D10ShaderResourceView*);

private:
	bool InitializeShader(ID3D10Device*, HWND, WCHAR*);
	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob*, HWND, WCHAR*);

	void SetShaderParameters(D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D10ShaderResourceView*);
	void RenderShader(ID3D10Device*, int);

private:
	ID3D10Effect* m_effect;
	ID3D10EffectTechnique* m_technique;
	ID3D10InputLayout* m_layout;

	ID3D10EffectMatrixVariable* m_pWorldMatrix;
	ID3D10EffectMatrixVariable* m_pViewMatrix;
	ID3D10EffectMatrixVariable* m_pProjectionMatrix;
	ID3D10EffectShaderResourceVariable* m_pTexture;
};