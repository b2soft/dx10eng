#pragma once

#include "CoreHeader.h"

class CGraphics;
class CInput;

class CSystem
{
public:
	CSystem();
	CSystem(const CSystem&);
	~CSystem();

	bool Initialize();
	void Run();
	void Shutdown();

	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

private:
	bool Frame();
	void InitializeWindows(int &, int &);
	void ShutdownWindows();

	LPCWSTR m_applicationName;
	HINSTANCE m_hinstance;
	HWND m_hwnd;

	CInput *m_Input;
	CGraphics *m_Graphics;

	CFpsMeter *m_Fps;
	CCpuMeter *m_Cpu;
	CTimer *m_Timer;
};

static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//globals

static CSystem *ApplicationHandle = 0;
