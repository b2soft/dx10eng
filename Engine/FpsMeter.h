#pragma once

#include "CoreHeader.h"

class CFpsMeter
{
public:
	CFpsMeter();
	CFpsMeter(const CFpsMeter &);
	~CFpsMeter();

	void Initialize();
	void Frame();
	int GetFps() const;

private:
	int m_fps, m_count;
	unsigned long m_startTime;
};
